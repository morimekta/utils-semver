package net.morimekta.semver;

import net.morimekta.semver.matcher.SemVerPlusMatcher;
import net.morimekta.semver.matcher.SemVerRangeMatcher;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static net.morimekta.semver.SemVer.MAX;
import static net.morimekta.semver.SemVer.NULL;
import static net.morimekta.semver.SemVer.SEMVER_CORE_SIMPLE;

/**
 * Simple interface to match a {@link SemVer} against some criteria.
 *
 */
public abstract class SemVerMatcher implements Predicate<SemVer> {
    /**
     * Parse a semantic matcher spec into an actual matcher. The
     * parse method supports three matcher variants:
     * <ul>
     *     <li><b>Maven-style range strings</b>: On the format of {@code [1.2.3,9.8.7)}</li>
     *     <li><b>APT and RPM like comparative strings</b>: With the format like {@code >=1.2.3}</li>
     *     <li><b>Simple version 'plus' syntax</b>: Like {@code 1.2+}</li>
     * </ul>
     *
     * @param spec The version matcher spec.
     * @return The matcher predicate.
     */
    public static SemVerMatcher parse(String spec) {
        spec = spec.strip();

        Matcher matcher = MAVEN_SPEC.matcher(spec);
        if (matcher.matches()) {
            SemVer min = NULL, max = MAX;
            if (matcher.group("min") != null) {
                min = SemVer.parse(matcher.group("min"));
            }
            if (matcher.group("max") != null) {
                max = SemVer.parse(matcher.group("max"));
            }
            if (min == NULL && max == MAX) {
                throw new IllegalArgumentException("Invalid range, no version: " + spec);
            }
            if (min.compareTo(max) > 0) {
                throw new IllegalArgumentException("Invalid range, reverse version order: " + spec);
            }
            return new SemVerRangeMatcher(min, "[".equals(matcher.group("pre")),
                                          max, "]".equals(matcher.group("post")));
        }

        matcher = COMPARE_SPEC.matcher(spec);
        if (matcher.matches()) {
            SemVer version = SemVer.parse(matcher.group("version"));
            switch (matcher.group("op")) {
                case "=":
                    return new SemVerRangeMatcher(version, true, version, true);
                case "<":
                    return new SemVerRangeMatcher(NULL, true, version, false);
                case "<=":
                    return new SemVerRangeMatcher(NULL, true, version, true);
                case ">":
                    return new SemVerRangeMatcher(version, false, MAX, true);
                case ">=":
                    return new SemVerRangeMatcher(version, true, MAX, true);
                default:
                    // Generally impossible to test.
                    throw new IllegalArgumentException("Invalid version operation: " + matcher.group("op"));
            }
        }

        matcher = PLUS_SPEC.matcher(spec);
        if (matcher.matches()) {
            if (matcher.group("patch") != null) {
                return new SemVerPlusMatcher(SemVer.of(
                        Integer.parseInt(matcher.group("major")),
                        Integer.parseInt(matcher.group("minor")),
                        Integer.parseInt(matcher.group("patch"))), SemVerLevel.PATCH);
            }
            if (matcher.group("minor") != null) {
                return new SemVerPlusMatcher(SemVer.of(
                        Integer.parseInt(matcher.group("major")),
                        Integer.parseInt(matcher.group("minor")),
                        0), SemVerLevel.MINOR);

            }
            return new SemVerPlusMatcher(SemVer.of(
                    Integer.parseInt(matcher.group("major")),
                    0, 0), SemVerLevel.MAJOR);
        }
        throw new IllegalArgumentException("Not a version spec: " + spec);
    }

    /**
     * Make a {@link SemVer} matcher instance checking against a minimum version.
     *
     * @param min The minimum version for the predicate.
     * @return The matcher predicate.
     */
    public static SemVerMatcher min(SemVer min) {
        return ofRange(min, SemVer.MAX);
    }

    /**
     * Make a {@link SemVer} matcher instance checking against a maximum version
     * to be below.
     *
     * @param max The version to be strictly below. Note that the diff is checked simply, so
     *            pre-releases will match in this case.
     * @return The matcher predicate.
     */
    public static SemVerMatcher below(SemVer max) {
        return ofRange(SemVer.NULL, max);
    }

    /**
     * Make a simple version range matcher with lower inclusive and upper exclusive versions.
     *
     * @param minInclusive The lower inclusive version.
     * @param maxExclusive The upper exclusive version.
     * @return The range matcher predicate.
     */
    public static SemVerMatcher ofRange(SemVer minInclusive, SemVer maxExclusive) {
        return new SemVerRangeMatcher(minInclusive, true, maxExclusive, false);
    }

    // ---- Private ----

    private static final Pattern MAVEN_SPEC   = Pattern
            .compile("(?<pre>[(\\[])" +
                     "(?<min>" + SEMVER_CORE_SIMPLE + ")?," +
                     "(?<max>" + SEMVER_CORE_SIMPLE + ")?" +
                     "(?<post>[)\\]])");
    private static final Pattern PLUS_SPEC    = Pattern
            .compile("(?<major>[0-9]+)(\\.(?<minor>[0-9]+)(\\.(?<patch>[0-9]+))?)?\\+");
    private static final Pattern COMPARE_SPEC = Pattern
            .compile("(?<op>(=|<|<=|>|>=))\\s*(?<version>" + SEMVER_CORE_SIMPLE + ")");
}
