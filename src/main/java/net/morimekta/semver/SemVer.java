package net.morimekta.semver;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

/**
 * <b>A full semantic versioning instance</b>. The SemVer class is immutable, so it
 * is safe to use as map keys etc. The SemVer parsing and {@link #toString()}
 * follows <a href="https://semver.org/">SemVer 2.0.0</a> strictly, but allow
 * extending to make localized variants where needed.
 * <p>
 * Semantic versioning has a pretty simple syntax, and this class should cover it
 * all, but does not have utilities for manipulating in detail, or strictly validating,
 * pre-release strings and build-info strings. The comparison sorting should follow
 * the spec to the detail.
 * <p>
 * The class has methods to manipulate the version in various ways, and will always
 * return a new {@link SemVer} instance with the desired changes.
 */
public class SemVer implements Comparable<SemVer> {
    public static final SemVer NULL = new SemVer(0, 0, 0, "", null);
    public static final SemVer MAX  = new SemVer(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, null, null);

    public static final Pattern SEMVER_CORE        = Pattern.compile(
            "(?<major>[0-9]+)[.](?<minor>[0-9]+)[.](?<patch>[0-9]+)");
    public static final Pattern SEMVER             = Pattern.compile(
            SEMVER_CORE +
            "(-(?<preRelease>[_a-zA-Z0-9][-._a-zA-Z0-9]*))?" +
            "(\\+(?<buildInfo>[_a-zA-Z0-9][-._a-zA-Z0-9]*))?");
    public static final Pattern SEMVER_CORE_SIMPLE = Pattern.compile(
            "([0-9]+)[.]([0-9]+)[.]([0-9]+)");

    /**
     * Parse a version string.
     *
     * @param version The version string to parse.
     * @return The SemVer instance.
     * @throws IllegalArgumentException If not a valid SemVer 2.0.0 version string.
     */
    public static SemVer parse(String version) {
        requireNonNull(version, "version == null");
        if (version.isEmpty()) {
            throw new IllegalArgumentException("Empty version");
        }

        Matcher matcher = SEMVER.matcher(version);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid semantic version string: " + version);
        }
        return new SemVer(Integer.parseInt(matcher.group("major")),
                          Integer.parseInt(matcher.group("minor")),
                          Integer.parseInt(matcher.group("patch")),
                          matcher.group("preRelease"),
                          matcher.group("buildInfo"));
    }

    /**
     * Create a {@link SemVer} instance with major minor and patch version.
     *
     * @param major The major version number.
     * @param minor The minor version number.
     * @param patch The patch version number.
     * @return The basic / core semantic version instance.
     * @throws IllegalArgumentException If any of the version numbers are negative.
     */
    public static SemVer of(int major, int minor, int patch) {
        return new SemVer(major, minor, patch, null, null);
    }

    public static SemVer of(int major, int minor, int patch, String preRelease, String buildInfo) {
        if (preRelease != null && preRelease.isEmpty()) {
            throw new IllegalArgumentException("Empty preRelease");
        }
        if (buildInfo != null && buildInfo.isEmpty()) {
            throw new IllegalArgumentException("Empty buildInfo");
        }
        return new SemVer(major, minor, patch, preRelease, buildInfo);
    }

    // ---- SemVer ----

    /**
     * @return The major version number.
     */
    public int major() {
        return major;
    }

    /**
     * @return The minor version number.
     */
    public int minor() {
        return minor;
    }

    /**
     * @return The patch version number.
     */
    public int patch() {
        return patch;
    }

    /**
     * @return The version pre-release string value or null if not present.
     */
    public String preRelease() {
        return preRelease;
    }

    /**
     * @return The version build-info string value or null if not present.
     */
    public String buildInfo() {
        return buildInfo;
    }

    /**
     * Do a simple increment on the lowest level that is set. This will increase
     * the pre-release (on patch level) if a pre-release is set, and will increment
     * the patch level if not.
     * <p>
     * See {@link #increment(SemVerLevel)} or {@link #incrementPreRelease(SemVerLevel, String)}
     * for details on the increment itself.
     *
     * @return The incremented version instance.
     */
    public SemVer increment() {
        if (preRelease != null) {
            return incrementPreRelease(SemVerLevel.PATCH, null);
        }
        return increment(SemVerLevel.PATCH);
    }

    /**
     * Calling this method will attempt making a release version of a pre-release
     * version.
     *
     * @return The release version without pre-release and build-info strings.
     * @throws IllegalStateException If not a pre-release version.
     */
    public SemVer release() {
        if (preRelease == null) {
            throw new IllegalStateException("Not a pre-release version");
        }
        return create(major, minor, patch, null, null);
    }

    /**
     * Do an increment of one of the core version numbers. This will reset all
     * lower-denominator version numbers to '0' and will remove both pre-release
     * and build info.
     * <p>
     * If the version was a pre-release at the specified level (meaning
     *
     * @param increment The increment level.
     * @return The incremented version instance.
     */
    public SemVer increment(SemVerLevel increment) {
        requireNonNull(increment, "increment == null");
        switch (increment) {
            case MAJOR:
                if (preRelease != null && minor == 0 && patch == 0) {
                    return create(major, 0, 0, null, null);
                }
                return create(major + 1, 0, 0, null, null);
            case MINOR:
                if (preRelease != null && patch == 0) {
                    return create(major, minor, 0, null, null);
                }
                return create(major, minor + 1, 0, null, null);
            case PATCH:
                if (preRelease != null) {
                    return create(major, minor, patch, null, null);
                }
                return create(major, minor, patch + 1, null, null);
        }
        // Generally impossible to test.
        throw new IllegalArgumentException("No increment handling for " + increment);
    }

    /**
     * Increment pre-release version, or make a pre-release out of this version by
     * preparing for the given increment. Calling this with a specific level and
     * release should be the same as calling {@link #increment(SemVerLevel)} with that
     * level directly.
     *
     * @param increment The planned increment level.
     * @param prefix The pre-release prefix string. This will be joined with a pre-release
     *               number using '.' separator.
     * @return The pre-release incremented version.
     */
    public SemVer incrementPreRelease(SemVerLevel increment, String prefix) {
        requireNonNull(increment, "increment == null");
        String preRelease = incrementPreReleaseString(prefix);
        switch (increment) {
            case MAJOR: {
                int newMajor = this.preRelease == null || minor > 0 || patch > 0 ? major + 1 : major;
                return create(newMajor, 0, 0, preRelease, null);
            }
            case MINOR: {
                int newMinor = this.preRelease == null || patch > 0 ? minor + 1 : minor;
                return create(major, newMinor, 0, preRelease, null);
            }
            case PATCH: {
                int newPatch = this.preRelease == null ? patch + 1 : patch;
                return create(major, minor, newPatch, preRelease, null);
            }
        }
        // Generally impossible to test.
        throw new IllegalArgumentException("No increment handling for " + increment);
    }

    /**
     * Specify pre-release string directly.
     *
     * @param preRelease The pre-release string.
     * @return Version with pre-release.
     */
    public SemVer withPreRelease(String preRelease) {
        requireNonNull(preRelease, "preRelease == null");
        if (preRelease.isEmpty()) {
            throw new IllegalArgumentException("Empty preRelease");
        }
        return create(major, minor, patch, preRelease, buildInfo);
    }

    /**
     * @return Version as is without pre-release.
     */
    public SemVer withoutPreRelease() {
        return create(major, minor, patch, null, buildInfo);
    }

    /**
     * Specify build-info string directly.
     *
     * @param buildInfo The build-info string.
     * @return Version with build-info.
     */
    public SemVer withBuildInfo(String buildInfo) {
        requireNonNull(buildInfo, "buildInfo == null");
        if (buildInfo.isEmpty()) {
            throw new IllegalArgumentException("Empty buildInfo");
        }
        return create(major, minor, patch, preRelease, buildInfo);
    }

    /**
     * @return Version as is without build-info.
     */
    public SemVer withoutBuildInfo() {
        return create(major, minor, patch, preRelease, null);
    }

    // ---- Comparable ----

    @Override
    public int compareTo(SemVer o) {
        if (major != o.major) {
            return Integer.compare(major, o.major);
        }
        if (minor != o.minor) {
            return Integer.compare(minor, o.minor);
        }
        if (patch != o.patch) {
            return Integer.compare(patch, o.patch);
        }
        if ((preRelease == null) != (o.preRelease() == null)) {
            return Boolean.compare(preRelease == null, o.preRelease == null);
        }
        if (preRelease != null) {
            // both have pre-release.
            String[] parts = preRelease.split("[.]");
            String[] other = o.preRelease.split("[.]");
            for (int i = 0; i < parts.length && i < other.length; ++i) {
                int c = compareParts(parts[i], other[i]);
                if (c != 0) return c;
            }
            if (parts.length != other.length) {
                return Integer.compare(other.length, parts.length);
            }
        }
        return 0;
    }

    // ---- Object ----

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SemVer semVer = (SemVer) o;
        return major == semVer.major &&
               minor == semVer.minor &&
               patch == semVer.patch &&
               Objects.equals(preRelease, semVer.preRelease) &&
               Objects.equals(buildInfo, semVer.buildInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(major, minor, patch, preRelease, buildInfo);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(major).append(".").append(minor).append(".").append(patch);
        if (preRelease != null) {
            builder.append("-").append(preRelease);
        }
        if (buildInfo != null) {
            builder.append("+").append(buildInfo);
        }
        return builder.toString();
    }

    // ---- PRIVATE ----

    private final int    major;
    private final int    minor;
    private final int    patch;
    private final String preRelease;
    private final String buildInfo;

    /**
     * Overridable creation method. This can be used to make sure version variants keep
     * the variant state when running increments and other changes.
     *
     * @param major The major version.
     * @param minor The minor version.
     * @param patch The patch version.
     * @param preRelease The pre-release string.
     * @param buildInfo The build-info string.
     * @return The created version instance.
     */
    protected SemVer create(int major, int minor, int patch, String preRelease, String buildInfo) {
        return new SemVer(major, minor, patch, preRelease, buildInfo);
    }

    protected SemVer(int major, int minor, int patch, String preRelease, String buildInfo) {
        if (major < 0) throw new IllegalArgumentException("Major " + major + " < 0");
        if (minor < 0) throw new IllegalArgumentException("Minor " + minor + " < 0");
        if (patch < 0) throw new IllegalArgumentException("Patch " + patch + " < 0");
        this.major = major;
        this.minor = minor;
        this.patch = patch;
        this.preRelease = preRelease;
        this.buildInfo = buildInfo;
    }

    private String incrementPreReleaseString(String prefix) {
        int preReleaseId = 0;
        if (preRelease != null) {
            if (INTEGER.matcher(preRelease).matches()) {
                preReleaseId = Integer.parseInt(preRelease);
                if (prefix == null) {
                    prefix = "";
                } else {
                    prefix += ".";
                }
            } else {
                Matcher matcher = PRE_RELEASE.matcher(preRelease);
                if (matcher.matches()) {
                    preReleaseId = Integer.parseInt(matcher.group("id"));
                    if (prefix == null) {
                        prefix = matcher.group("prefix");
                    } else {
                        String pfg = matcher.group("prefix");
                        if (pfg != null && !prefix.equals(pfg.substring(0, pfg.length() - 1))) {
                            // Changing the prefix resets the number.
                            preReleaseId = 0;
                        }
                        prefix += ".";
                    }
                } else {
                    prefix = preRelease + ".";
                }
            }
        } else if (prefix != null) {
            if (!prefix.endsWith(".") && !prefix.endsWith("-")) {
                prefix += ".";
            }
        }

        ++preReleaseId;
        if (prefix == null) {
            return String.valueOf(preReleaseId);
        } else {
            return prefix + preReleaseId;
        }
    }

    private static int compareParts(String a, String b) {
        Matcher am = INTEGER.matcher(a);
        Matcher bm = INTEGER.matcher(b);
        if (am.matches() && bm.matches()) {
            return Integer.compare(Integer.parseInt(a), Integer.parseInt(b));
        } else if (am.matches()) {
            return -1;
        } else if (bm.matches()) {
            return 1;
        }
        return a.compareTo(b);
    }

    private static final Pattern INTEGER     = Pattern.compile("^[0-9]+$");
    private static final Pattern PRE_RELEASE = Pattern.compile(
            "^(?<prefix>([a-zA-Z0-9][_a-zA-Z0-9]*[-.])*)" +
            "(?<id>[0-9]+)$");
}
