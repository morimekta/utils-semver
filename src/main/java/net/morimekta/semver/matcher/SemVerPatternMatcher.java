package net.morimekta.semver.matcher;

import net.morimekta.semver.SemVer;
import net.morimekta.semver.SemVerMatcher;

import java.util.Objects;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

/**
 * Check version string (using toString) against a regexp pattern.
 * This does not have a separate parser, as it is difficult to
 * parse a range <i>or</i> regexp without some weird rules.
 */
public class SemVerPatternMatcher extends SemVerMatcher {
    private final Pattern pattern;

    public SemVerPatternMatcher(Pattern pattern) {
        requireNonNull(pattern, "pattern == null");
        this.pattern = pattern;
    }

    @Override
    public boolean test(SemVer semVer) {
        return pattern.matcher(semVer.toString()).find();
    }

    // --- Object ---

    @Override
    public String toString() {
        return pattern.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SemVerPatternMatcher that = (SemVerPatternMatcher) o;
        return pattern.equals(that.pattern);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pattern);
    }
}
