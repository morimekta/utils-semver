package net.morimekta.semver.matcher;

import net.morimekta.semver.SemVer;
import net.morimekta.semver.SemVerMatcher;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Matches a range like supporting maven versions, or comparative like
 * some package management systems (e.g. APT). This only supports a single
 * range, open or closed.
 *
 * @see SemVerMatcher#parse(String) for parsing details.
 */
public class SemVerRangeMatcher extends SemVerMatcher {
    private final SemVer min;
    private final SemVer max;
    private final boolean minInclusive;
    private final boolean maxInclusive;

    public SemVerRangeMatcher(SemVer min, boolean minInclusive, SemVer max, boolean maxInclusive) {
        requireNonNull(min, "min == null");
        requireNonNull(max, "max == null");
        this.min = min;
        this.minInclusive = minInclusive;
        this.max = max;
        this.maxInclusive = maxInclusive;
    }

    @Override
    public boolean test(SemVer semVer) {
        return semVer.compareTo(min) >= (minInclusive ? 0 : 1) &&
               semVer.compareTo(max) <= (maxInclusive ? 0 : -1);
    }

    // --- Object ---

    @Override
    public String toString() {
        if (min == SemVer.NULL) {
            return (maxInclusive ? "<=" : "<") + max;
        }
        if (max == SemVer.MAX) {
            return (minInclusive ? ">=" : ">") + min;
        }
        return (minInclusive ? "[" : "(") + min + "," + max + (maxInclusive ? "]" : ")");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SemVerRangeMatcher that = (SemVerRangeMatcher) o;
        return minInclusive == that.minInclusive &&
               maxInclusive == that.maxInclusive &&
               min.equals(that.min) &&
               max.equals(that.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max, minInclusive, maxInclusive);
    }
}
