package net.morimekta.semver.matcher;

import net.morimekta.semver.SemVer;
import net.morimekta.semver.SemVerLevel;
import net.morimekta.semver.SemVerMatcher;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Matcher for the maven like matcher pattern of '1.2+', meaning being
 * in the range of '1.2.0' but less than '2.0.0', and not a patch version.
 *
 * @see SemVerMatcher#parse(String) for parsing details.
 */
public class SemVerPlusMatcher extends SemVerMatcher {
    private final SemVer      minSpec;
    private final SemVerLevel plusAtLevel;

    public SemVerPlusMatcher(SemVer minSpec, SemVerLevel plusAtLevel) {
        requireNonNull(minSpec, "minSpec == null");
        requireNonNull(plusAtLevel, "plusAtLevel == null");
        this.minSpec = minSpec;
        this.plusAtLevel = plusAtLevel;
    }

    @Override
    public boolean test(SemVer semVer) {
        if (semVer.preRelease() != null) {
            return false;
        }
        switch (plusAtLevel) {
            case MAJOR:
                return semVer.major() >= minSpec.major();
            case MINOR:
                return semVer.major() == minSpec.major() &&
                       semVer.minor() >= minSpec.minor();
            case PATCH:
            default:
                return semVer.major() == minSpec.major() &&
                       semVer.minor() == minSpec.minor() &&
                       semVer.patch() >= minSpec.patch();
        }
    }

    // ---- Object ----

    @Override
    public String toString() {
        switch (plusAtLevel) {
            case MAJOR:
                return minSpec.major() + "+";
            case MINOR:
                return minSpec.major() + "." + minSpec.minor() + "+";
            case PATCH:
            default:
                return minSpec.major() + "." + minSpec.minor() + "." + minSpec.patch() + "+";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SemVerPlusMatcher that = (SemVerPlusMatcher) o;
        return minSpec.equals(that.minSpec) &&
               plusAtLevel == that.plusAtLevel;
    }

    @Override
    public int hashCode() {
        return Objects.hash(minSpec, plusAtLevel);
    }
}
