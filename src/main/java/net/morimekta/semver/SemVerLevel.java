package net.morimekta.semver;

/**
 * The version level points to a specific position in the core version. There is
 * one level for each of {@code [ major, minor, patch ]} version numbers. This is
 * e.g. used when incrementing versions.
 */
public enum SemVerLevel {
    /**
     * Major versions denote breaking changes. New full code replacements etc that
     * have no backward compatibility guarantees.
     */
    MAJOR,
    /**
     * Minor versions denote additive changes that for compatibility will require a
     * minimum version, but is still compatible with older versions.
     */
    MINOR,
    /**
     * Patch versions denote changes not visible on the interfaces of the library
     * or service.
     */
    PATCH,
}
