package net.morimekta.semver;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class SemVerMatcherTest {
    @Test
    public void testBadParse() {
        try {
            SemVerMatcher.parse("foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a version spec: foo"));
        }
        try {
            SemVerMatcher.parse("[,]");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid range, no version: [,]"));
        }
        try {
            SemVerMatcher.parse("[2.0.0,1.2.3]");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid range, reverse version order: [2.0.0,1.2.3]"));
        }
    }

    @Test
    public void testParse() {
        assertThat(SemVerMatcher.parse("[1.2.3,]"), is(SemVerMatcher.parse(">=1.2.3")));
        assertThat(SemVerMatcher.parse("(1.2.3,]"), is(SemVerMatcher.parse(">1.2.3")));
        assertThat(SemVerMatcher.parse("[,1.2.3]"), is(SemVerMatcher.parse("<=1.2.3")));
        assertThat(SemVerMatcher.parse("[,1.2.3)"), is(SemVerMatcher.parse("<1.2.3")));
        assertThat(SemVerMatcher.parse("[1.2.3,1.2.3]"), is(SemVerMatcher.parse("=1.2.3")));
    }
}
