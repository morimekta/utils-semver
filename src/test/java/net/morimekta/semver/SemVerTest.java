package net.morimekta.semver;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static net.morimekta.semver.SemVer.of;
import static net.morimekta.semver.SemVer.parse;
import static net.morimekta.semver.SemVerLevel.MAJOR;
import static net.morimekta.semver.SemVerLevel.MINOR;
import static net.morimekta.semver.SemVerLevel.PATCH;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class SemVerTest {
    @Test
    public void testParse() {
        SemVer parsed = parse("1.2.3-foo+bar");
        assertThat(parsed.major(), is(1));
        assertThat(parsed.minor(), is(2));
        assertThat(parsed.patch(), is(3));
        assertThat(parsed.preRelease(), is("foo"));
        assertThat(parsed.buildInfo(), is("bar"));
        assertThat(parsed.toString(), is("1.2.3-foo+bar"));
        assertThat(parsed, is(of(1, 2, 3, "foo", "bar")));

        assertThat(parsed.withoutBuildInfo(),
                   is(of(1, 2, 3, "foo", null)));
        assertThat(parsed.withBuildInfo("fizz"),
                   is(of(1, 2, 3, "foo", "fizz")));
        assertThat(parsed.withoutPreRelease(),
                   is(of(1, 2, 3, null, "bar")));
        assertThat(parsed.withPreRelease("buzz"),
                   is(of(1, 2, 3, "buzz", "bar")));
        assertThat(parsed.withoutBuildInfo().withoutPreRelease(),
                   is(of(1, 2, 3)));

        try {
            parse("f00.bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Invalid semantic version string: f00.bar"));
        }
        try {
            parse("");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty version"));
        }

        assertThat(parsed.release(), is(SemVer.of(1, 2, 3)));
    }

    @Test
    public void testHashCode() {
        assertThat(parse("1.2.3").hashCode(), is(parse("1.2.3").hashCode()));
        assertThat(parse("1.2.3").hashCode(), is(not(parse("1.2.3-foo+bar").hashCode())));
    }

    @Test
    public void testBadArgs() {
        SemVer version = parse("1.2.3-foo+bar");

        try {
            version.withPreRelease(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("preRelease == null"));
        }
        try {
            version.withPreRelease("");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty preRelease"));
        }

        try {
            version.withBuildInfo(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("buildInfo == null"));
        }
        try {
            version.withBuildInfo("");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty buildInfo"));
        }

        try {
            SemVer.of(-1, 2, 3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Major -1 < 0"));
        }
        try {
            SemVer.of(1, -2, 3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Minor -2 < 0"));
        }
        try {
            SemVer.of(1, 2, -3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Patch -3 < 0"));
        }
        try {
            SemVer.of(1, 2, 3, "", "foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty preRelease"));
        }
        try {
            SemVer.of(1, 2, 3, "foo", "");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty buildInfo"));
        }
        try {
            SemVer.of(1, 2, 3).release();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("Not a pre-release version"));
        }
    }

    public static Stream<Arguments> testIncrementData() {
        return Stream.of(arguments(parse("1.2.3-foo"), parse("1.2.3-foo.1")),
                         arguments(parse("1.2.3-foo.2"), parse("1.2.3-foo.3")),
                         arguments(parse("1.2.3"), parse("1.2.4")));
    }

    @ParameterizedTest
    @MethodSource("testIncrementData")
    public void testIncrement(SemVer from, SemVer expected) {
        assertThat(from.increment(), is(expected));
    }

    public static Stream<Arguments> testIncrementLevelData() {
        return Stream.of(arguments(parse("1.2.3"), PATCH, parse("1.2.4")),
                         arguments(parse("1.2.3-foo"), PATCH, parse("1.2.3")),
                         arguments(parse("1.2.3"), MINOR, parse("1.3.0")),
                         arguments(parse("1.2.0"), MINOR, parse("1.3.0")),
                         arguments(parse("1.2.3-foo"), MINOR, parse("1.3.0")),
                         arguments(parse("1.2.0-foo"), MINOR, parse("1.2.0")),
                         arguments(parse("1.2.3"), MAJOR, parse("2.0.0")),
                         arguments(parse("1.2.0"), MAJOR, parse("2.0.0")),
                         arguments(parse("1.0.0"), MAJOR, parse("2.0.0")),
                         arguments(parse("1.2.3-foo"), MAJOR, parse("2.0.0")),
                         arguments(parse("1.2.0-foo"), MAJOR, parse("2.0.0")),
                         arguments(parse("1.0.3-foo"), MAJOR, parse("2.0.0")),
                         arguments(parse("1.0.0-foo"), MAJOR, parse("1.0.0")));
    }

    @ParameterizedTest
    @MethodSource("testIncrementLevelData")
    public void testIncrementLevel(SemVer from, SemVerLevel level, SemVer expected) {
        assertThat(from.increment(level), is(expected));
    }
    public static Stream<Arguments> testIncrementPreReleaseData() {
        return Stream.of(arguments(parse("1.2.3"), PATCH, null, parse("1.2.4-1")),
                         arguments(parse("1.2.3"), PATCH, "foo", parse("1.2.4-foo.1")),
                         arguments(parse("1.2.3-foo"), PATCH, null, parse("1.2.3-foo.1")),
                         arguments(parse("1.2.3-foo.2"), PATCH, null, parse("1.2.3-foo.3")),
                         arguments(parse("1.2.3-foo.2"), PATCH, "foo", parse("1.2.3-foo.3")),
                         arguments(parse("1.2.3-foo.2"), PATCH, "bar", parse("1.2.3-bar.1")),
                         arguments(parse("1.2.3-2"), PATCH, "bar", parse("1.2.3-bar.3")),
                         arguments(parse("1.2.3-2"), PATCH, null, parse("1.2.3-3")),
                         arguments(parse("1.2.3"), MINOR, null, parse("1.3.0-1")),
                         arguments(parse("1.2.0"), MINOR, null, parse("1.3.0-1")),
                         arguments(parse("1.2.3-foo"), MINOR, null, parse("1.3.0-foo.1")),
                         arguments(parse("1.2.0-foo"), MINOR, null, parse("1.2.0-foo.1")),
                         arguments(parse("1.2.3"), MAJOR, null, parse("2.0.0-1")),
                         arguments(parse("1.2.0"), MAJOR, null, parse("2.0.0-1")),
                         arguments(parse("1.0.0"), MAJOR, null, parse("2.0.0-1")),
                         arguments(parse("1.2.3-foo"), MAJOR, null, parse("2.0.0-foo.1")),
                         arguments(parse("1.2.0-foo"), MAJOR, null, parse("2.0.0-foo.1")),
                         arguments(parse("1.0.3-foo"), MAJOR, null, parse("2.0.0-foo.1")),
                         arguments(parse("1.0.0-foo"), MAJOR, null, parse("1.0.0-foo.1")));
    }

    @ParameterizedTest
    @MethodSource("testIncrementPreReleaseData")
    public void testIncrementPreRelease(SemVer from, SemVerLevel level, String prefix, SemVer expected) {
        assertThat(from.incrementPreRelease(level, prefix), is(expected));
        assertThat(from.incrementPreRelease(level, prefix).release(), is(from.increment(level)));
    }

    public static Stream<Arguments> testCompareData() {
        return Stream.of(arguments(parse("1.2.3"), parse("2.3.4"), -1),
                         arguments(parse("1.2.3"), parse("1.3.4"), -1),
                         arguments(parse("1.2.3"), parse("1.2.4"), -1),
                         arguments(parse("1.2.3"), parse("1.2.3"), 0),
                         arguments(parse("1.2.3"), parse("1.2.3-foo"), 1),
                         arguments(parse("1.2.3-bar"), parse("1.2.3-foo"), -4),
                         arguments(parse("1.2.3-foo.1"), parse("1.2.3-foo.2"), -1),
                         arguments(parse("1.2.3-foo"), parse("1.2.3-foo.1"), 1),
                         arguments(parse("1.2.3-foo.bar"), parse("1.2.3-foo.1"), 1),
                         arguments(parse("1.2.3-foo.1"), parse("1.2.3-foo.FOO"), -1));
    }

    @ParameterizedTest
    @MethodSource("testCompareData")
    public void testCompare(SemVer a, SemVer b, int compared) {
        assertThat(a.compareTo(b), is(compared));
        assertThat(b.compareTo(a), is(-compared));
    }

    @Test
    public void testSorting() {
        assertThat(Stream.of(parse("2.0.0"),
                             parse("1.2.3"),
                             parse("1.2.3+baz"),
                             parse("1.2.3-foo.1"),
                             parse("1.2.3-foo.bar"))
                         .sorted()
                         .collect(toList()),
                   is(List.of(
                           parse("1.2.3-foo.1"),
                           parse("1.2.3-foo.bar"),
                           parse("1.2.3"),
                           parse("1.2.3+baz"),
                           parse("2.0.0"))));
    }
}
