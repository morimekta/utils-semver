package net.morimekta.semver.matcher;

import net.morimekta.semver.SemVer;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class SemVerPatternMatcherTest {
    @Test
    public void testMin() {
        Predicate<SemVer> min = new SemVerPatternMatcher(Pattern.compile("-foo$"));
        assertThat(min.test(SemVer.parse("1.2.3")), is(false));
        assertThat(min.test(SemVer.parse("1.2.3-foo")), is(true));
        assertThat(min.hashCode(), is(min.hashCode()));
        assertThat(min.toString(), is("-foo$"));
        Predicate<SemVer> max = new SemVerPatternMatcher(Pattern.compile("^1\\.2\\."));
        assertThat(max, is(max));
        assertThat(max, is(not(min)));
        assertThat(max.toString(), is("^1\\.2\\."));
        assertThat(max.hashCode(), is(max.hashCode()));
        assertThat(max.hashCode(), is(not(min.hashCode())));
    }
}
