package net.morimekta.semver.matcher;

import net.morimekta.semver.SemVer;
import net.morimekta.semver.SemVerMatcher;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static net.morimekta.semver.SemVer.parse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class SemVerRangeMatcherTest {
    @Test
    public void testMin() {
        Predicate<SemVer> min = SemVerMatcher.min(parse("1.2.3"));
        assertThat(min.test(parse("0.0.0")), is(false));
        assertThat(min.test(parse("1.2.2")), is(false));
        assertThat(min.test(parse("1.2.3")), is(true));
        assertThat(min.test(parse("101.202.303")), is(true));
        assertThat(min.toString(), is(">=1.2.3"));
        assertThat(min.hashCode(), is(min.hashCode()));
    }

    @Test
    public void testBelow() {
        Predicate<SemVer> max = SemVerMatcher.below(parse("1.2.3"));
        assertThat(max.test(parse("0.0.0")), is(true));
        assertThat(max.test(parse("1.2.2")), is(true));
        assertThat(max.test(parse("1.2.3")), is(false));
        assertThat(max.test(parse("101.202.303")), is(false));
        assertThat(max.toString(), is("<1.2.3"));
        assertThat(max.hashCode(), is(max.hashCode()));
        assertThat(max, is(max));
        Predicate<SemVer> range = SemVerMatcher.ofRange(parse("1.2.3"), parse("2.0.0"));
        assertThat(max, is(not(range)));
        assertThat(max.hashCode(), is(not(range.hashCode())));
    }

    @Test
    public void testRange() {
        Predicate<SemVer> range = SemVerMatcher.ofRange(parse("1.2.3"), parse("2.0.0"));
        assertThat(range.test(parse("0.0.0")), is(false));
        assertThat(range.test(parse("1.2.2")), is(false));
        assertThat(range.test(parse("1.2.3")), is(true));
        assertThat(range.test(parse("1.9999.9999")), is(true));
        assertThat(range.test(parse("2.0.0")), is(false));
        assertThat(range.test(parse("101.202.303")), is(false));
        assertThat(range.toString(), is("[1.2.3,2.0.0)"));
        assertThat(range.hashCode(), is(range.hashCode()));
    }
}
