package net.morimekta.semver.matcher;

import net.morimekta.semver.SemVer;
import net.morimekta.semver.SemVerMatcher;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class SemVerPlusMatcherTest {
    @Test
    public void testSpecMajor() {
        Predicate<SemVer> major = SemVerMatcher.parse("2+");
        assertThat(major.test(SemVer.parse("1.2.3")), is(false));
        assertThat(major.test(SemVer.parse("2.0.0")), is(true));
        assertThat(major.test(SemVer.parse("22.0.0")), is(true));
        assertThat(major.toString(), is("2+"));
    }

    @Test
    public void testSpecMinor() {
        Predicate<SemVer> minor = SemVerMatcher.parse("1.2+");
        assertThat(minor.test(SemVer.parse("0.1.2")), is(false));
        assertThat(minor.test(SemVer.parse("1.1.2")), is(false));
        assertThat(minor.test(SemVer.parse("1.2.0")), is(true));
        assertThat(minor.test(SemVer.parse("1.22.0")), is(true));
        assertThat(minor.test(SemVer.parse("2.0.0")), is(false));
        assertThat(minor.toString(), is("1.2+"));
    }

    @Test
    public void testSpecPatch() {
        Predicate<SemVer> patch = SemVerMatcher.parse("1.2.3+");
        assertThat(patch.test(SemVer.parse("0.1.2")), is(false));
        assertThat(patch.test(SemVer.parse("1.1.2")), is(false));
        assertThat(patch.test(SemVer.parse("1.2.2")), is(false));
        assertThat(patch.test(SemVer.parse("1.2.3")), is(true));
        assertThat(patch.test(SemVer.parse("1.2.4-foo")), is(false));
        assertThat(patch.test(SemVer.parse("1.2.99")), is(true));
        assertThat(patch.test(SemVer.parse("1.3.0")), is(false));
        assertThat(patch.test(SemVer.parse("2.0.0")), is(false));
        assertThat(patch.toString(), is("1.2.3+"));

        Predicate<SemVer> minor = SemVerMatcher.parse("1.2+");
        assertThat(patch, is(patch));
        assertThat(patch, is(not(minor)));
        assertThat(patch.hashCode(), is(patch.hashCode()));
        assertThat(patch.hashCode(), is(not(minor.hashCode())));
    }

    @Test
    public void testBadParse() {
        try {
            SemVerMatcher.parse("foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a version spec: foo"));
        }
    }
}
