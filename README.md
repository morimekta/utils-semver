Semantic Versioning
===================

Utility for handling [semantic versioning](https://semver.org). This project
is meant to be used where versions must be handled programmatically, e.g.
where managing client differences based on their version. In case the version
strings are not compatible with `semver 2.0.0`, the classes are extendable so
parsing and stringification can be customized where needed.

The code in this project is meant to follow the 2.0.0 syntax rather strictly,
though there are some points of leniency, specifically in the pre-release and
build-info string syntax.

Examples can be:

- A client configuration service that needs to handle different client versions
  different in some way.
- A build system that needs to do detailed version updates (increments) runtime.
- A deployment system that needs to handle versions of deployed services.